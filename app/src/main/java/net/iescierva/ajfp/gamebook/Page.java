package net.iescierva.ajfp.gamebook;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.Locale;

//TODO La clase Page debería asumir toda la responsabilidad posible del código que manipula sus instancias
public class Page {
    protected String text;
    protected String title;
    protected String btn1Text;
    protected String btn2Text;
    protected boolean loaded;
    protected boolean twoButtons;

    public Page(String title, String text, String btn1Text, String btn2Text) {
        this.text = text;
        this.title = title;
        this.btn1Text = btn1Text;
        this.btn2Text = btn2Text;
        this.twoButtons = true;
        this.loaded = false;
    }

    public Page(String title, String text, String btn1Text) {
        this.text = text;
        this.title = title;
        this.btn1Text = btn1Text;
        this.twoButtons = false;
        this.loaded = false;
    }

    public void loadImage(ImageView imgView) {
        //Uso la librería Picasso por los problemas encontrados con los BitMaps
        Picasso.get().load("file:///android_asset/images/" + this.title + ".png").into(imgView);
    }

    public MediaPlayer startMusic(Activity activity, long duration) {
        String nombCancion = title.replaceAll(" |:", "_")
                .toLowerCase(Locale.ROOT)
                .replace("á", "a")
                .replace("é", "e")
                .replace("í", "i")
                .replace("ó", "o")
                .replace("ú", "u");

        int idAudio = activity.getResources().getIdentifier(nombCancion, "raw", activity.getPackageName());
        if (idAudio != 0) {
            final MediaPlayer audio = MediaPlayer.create(activity, idAudio);
            if (duration - 60000 > 0) {
                new CountDownTimer(duration - 60000, duration) {
                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {
                        audio.start();
                    }
                }.start();
            } else {
                audio.start();
            }
            return audio;
        }
        return null;
    }
}
package net.iescierva.ajfp.gamebook;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity {
    protected Map<String, Page> pageList;
    protected AssetManager assManager;
    protected SharedPreferences defaultPrefManager;
    protected SharedPreferences.Editor prefEditor;
    private ScrollView scrollView;
    private Button btn1, btn2;
    private ImageView imgView;
    private Page currentPage;
    private long startTime;
    private TextView text;
    private MediaPlayer audio;
    private final Button.OnClickListener changePage = view -> {
        /* Extraemos del diccionario la página de la última sesión y la usamos para
        cambiar el texto o la visibilidad de ambos botones, la visibilidad o imagen de la
        ImageView y el texto de la página. */

        Button button = (Button) view;
        Animation fadeAnim = AnimationUtils.loadAnimation(this, R.anim.fade_anim);
        scrollView.scrollTo(0, 0);
        String title = button.getText().toString();
        if (title.equals("He terminado")) {
            float userSecsPerChar = ((System.currentTimeMillis() - startTime) / 1000F) / text.length();
            prefEditor.putFloat("readingSpeed", userSecsPerChar);
            prefEditor.commit();
        }
        currentPage = pageList.get(title);

        fadeAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                btn1.setVisibility(View.GONE);
                btn2.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                loadPage(currentPage);
            }
        });
        findViewById(R.id.scrollView).startAnimation(fadeAnim);

    };
    private Dialog dialog;

    private void startMusic_ShowButtons() {
        int numCharsPage = currentPage.text.length();
        long pageDuration = (long) BigDecimal.valueOf(numCharsPage
                * defaultPrefManager.getFloat("readingSpeed", 0))
                .setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue() * 1000;
        new CountDownTimer(pageDuration, pageDuration) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                makeButtonsVisible();
                currentPage.loaded = true;
                updatePagelist();
            }
        }.start();
        //Sólo las páginas con final reproducirán música
        if (!currentPage.twoButtons && !currentPage.title.contains("Continuar")) {
            System.out.println(currentPage.title);
            audio = currentPage.startMusic(MainActivity.this, pageDuration);
        }
    }

    private void makeButtonsVisible() {
        btn1.setVisibility(View.VISIBLE);
        if (currentPage.twoButtons)
            btn2.setVisibility(View.VISIBLE);
    }

    private void loadPage(Page page) {
        text.setVisibility(View.VISIBLE);
        if (page.title.equals("Prueba de lectura")) {
            startTime = System.currentTimeMillis();
        }
        text.setText(page.text);
        //La imagen también se debe sincronizar para que aparezca sólo cuándo termine la lectura
        page.loadImage(imgView);
        btn1.setText(page.btn1Text);
        btn2.setText(page.btn2Text);
        /* TODO Aquí hay un bosquejo de cómo implementar el modo de pago, ahora mismo por
        defecto las páginas que se hayan leído ya se pueden saltar */
        currentPage = pageList.get(page.title);
        prefEditor.putString("currentPage", page.title);
        prefEditor.commit();
        if (!page.loaded) {
            startMusic_ShowButtons();
        } else {
            makeButtonsVisible();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Con estas dos líneas podemos controlar el volumen de la aplicación
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 4, 0);
        super.onCreate(savedInstanceState);
        assManager = getAssets();
        defaultPrefManager = PreferenceManager.getDefaultSharedPreferences(this);
        prefEditor = defaultPrefManager.edit();

         /*Extraigo el json de pageList y usando la clase Gson lo convierto en HashMap. La
        segunda línea es necesaria para guardar las clases que se pierden en
        tiempo de compilación, como Page. Con esto cualquier estructura de datos se puede
        almacenar en el móvil y no es necesario crearla cada vez */
        pageList = new Gson().fromJson(defaultPrefManager.getString("pageList", ""),
                new TypeToken<HashMap<String, Page>>() {
                }.getType());
        setContentView(R.layout.activity_main);

        dialog = new Dialog(this);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        text = findViewById(R.id.textView);
        imgView = findViewById(R.id.imageView);
        scrollView = findViewById(R.id.scrollView);

        if (!checkFirstBookLoad())
            getProtName();
        else {
            // Cambiar esta línea para probar algo en una página en particular, introducir
            // defaultPrefManager.getString("currentPage", null) en el get para restaurar
            currentPage = pageList.get(defaultPrefManager.getString("currentPage", null));
            loadPage(currentPage);
        }

        btn1.setOnClickListener(changePage);
        btn2.setOnClickListener(changePage);
    }

    private boolean checkFirstBookLoad() {
        boolean bookLoaded = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean("bookLoaded", false);
        if (!bookLoaded) {
            prefEditor.putBoolean("bookLoaded", true);
        }
        return bookLoaded;
    }

    private void readProcessBook() {
        try {
            pageList = new HashMap<>();
            //Lectura y conversión con regex y lógica del libro en un diccionario
            InputStream stream = (assManager.open("book.txt"));
            String result = new BufferedReader(
                    new InputStreamReader(stream, StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining("\n"));
            String[] textArray = result.split("#\n\n#");
            int counter = 0;
            String[] div = new String[0];
            for (String string : textArray) {
                string = ensureFormat(string);
                ++counter;
                if (counter % 2 != 0) {
                    div = string.split("\\|");
                } else {
                    String text = string;
                    Page page;
                    if (div.length > 2)
                        page = new Page(div[0], text, div[1], div[2]);
                    else {
                        //Para probar errores al modificar el libro: System.out.println(div[0]);
                        page = new Page(div[0], text, div[1]);
                        page.twoButtons = false;
                    }
                    pageList.put(div[0], page);
                }
            }
            //Guardado del libro convertido en un diccionario de título:páginas en memoria del móvil
            updatePagelist();
            loadPage(pageList.get("Prueba de lectura"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getProtName() {
        dialog.setContentView(R.layout.custom_dialog);
        //Es necesario hacer transparente el fondo del dialog porque ya tenemos uno en el layout
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //Una vez se le asigna un layout a una vista podemos acceder a sus hijos
        Button btnOK = dialog.findViewById(R.id.button);
        EditText eText = dialog.findViewById(R.id.editTextProt);
        btnOK.setOnClickListener(view -> {
            prefEditor.putString("protName", eText.getText().toString());
            prefEditor.commit();
            dialog.dismiss();
            readProcessBook();
        });
        dialog.show();
    }

    private String ensureFormat(String text) {
        text = text.replace("Iván", defaultPrefManager.getString("protName", null));
        text = text.replaceAll("“|”", "\"");
        text = text.replaceAll("\"\n\"", "\"\n\n\"");
        text = text.replaceAll(".\n\"", ".\n\n\"");
        text = text.replaceAll("\".\n\"", "\".\n\n\"");
        return text;
    }

    private void updatePagelist() {
        String json = new Gson().toJson(pageList);
        prefEditor.putString("pageList", json);
        prefEditor.commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (audio != null) audio.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (audio != null) audio.start();
    }
}